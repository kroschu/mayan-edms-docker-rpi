# Empty base.py to allow local.py to run
from mayan.settings.docker import *  # NOQA

CELERY_CREATE_MISSING_QUEUES = True
