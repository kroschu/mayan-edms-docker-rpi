# Mayan EDMS Docker image for Raspberry Pi

This is a fork of the [Mayan EDMS Docker image](https://gitlab.com/mayan-edms/mayan-edms-docker/) updated to be run on a Raspberry Pi.

To build the image run the following command:

```console
$ docker create volume mayan_data
$ docker build --tag rpi-mayan-edms:2.7.3 .
$ docker run -d -p 80:80 --name mayan -v mayan_data:/var/lib/mayan --restart always rpi-mayan-edms:2.7.3
```